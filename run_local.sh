#!/bin/sh

./mvnw clean package -Plocal
java -jar -Dspring.active.profiles=local target/holiday-service-api.jar
