FROM adoptopenjdk/openjdk15:alpine-jre

WORKDIR /opt

COPY target/holiday-service-api.jar application.jar

ENTRYPOINT ["java", "-jar", "application.jar"]