package com.pm.example.api.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CountryDto implements Serializable  {

    private String key;
    private String value;
}
