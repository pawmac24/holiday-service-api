package com.pm.example.api.service;

import com.pm.example.api.dto.CountryDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RefreshScope
@Slf4j
@RequiredArgsConstructor
public class CountryValidatorService {

    private final RestTemplate restTemplate;

    @Value("${url.available.countries}")
    private String availableCountriesUrl;

    public boolean isCountryValid(String countryCode) {
        ResponseEntity<List<CountryDto>> availableCountriesResponse = restTemplate.exchange(
                availableCountriesUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );

        List<CountryDto> countryDtos = availableCountriesResponse.getBody();

        if (countryDtos == null) {
            return false;
        }

        boolean countryCodeFound = countryDtos.stream().anyMatch(c -> countryCode.equals(c.getKey()));
        if (!countryCodeFound) {
            String availableCountryCodes = countryDtos.stream()
                .map(CountryDto::getKey).
                collect(Collectors.joining(", "));
            log.info("Choose one of available countryCodes {}", availableCountryCodes);
        }
        return countryCodeFound;
    }
}
