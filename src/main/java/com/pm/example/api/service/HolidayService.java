package com.pm.example.api.service;

import com.pm.example.api.dto.HolidayResponse;
import com.pm.example.api.dto.PublicHolidayDto;
import com.pm.example.api.exception.CountryNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RefreshScope
@Slf4j
@RequiredArgsConstructor
public class HolidayService {

    private final RestTemplate restTemplate;
    private final CountryValidatorService countryValidatorService;

    @Value("${url.next.holidays}")
    private String nextHolidaysUrl;

    public HolidayResponse getNextCommonHoliday(LocalDate startDate, String countryCode1, String countryCode2) {
        if (!countryValidatorService.isCountryValid(countryCode1)) {
            log.error("Country {} not found", countryCode1);
            throw new CountryNotFoundException("Country " + countryCode1 + " not found");
        }
        if (!countryValidatorService.isCountryValid(countryCode2)) {
            log.error("Country {} not found", countryCode2);
            throw new CountryNotFoundException("Country " + countryCode2 + " not found");
        }

        List<PublicHolidayDto> country1Holidays = getHolidaysForCountry(countryCode1);
        List<PublicHolidayDto> country2Holidays = getHolidaysForCountry(countryCode2);

        List<LocalDate> country1HolidayDates = getHolidayDates(country1Holidays);
        List<LocalDate> country2HolidayDates = getHolidayDates(country2Holidays);

        LocalDate firstCommonDate = country1HolidayDates.stream()
                .filter(h -> h.isAfter(startDate))
                .filter(country2HolidayDates::contains)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No common holiday dates"));

        String country1HolidayName = extractHolidayName(country1Holidays, firstCommonDate);
        String country2HolidayName = extractHolidayName(country2Holidays, firstCommonDate);

        log.info("Response for holiday {} found", firstCommonDate);

        return HolidayResponse.builder()
                .date(firstCommonDate)
                .name1(country1HolidayName)
                .name2(country2HolidayName)
                .build();
    }

    private List<LocalDate> getHolidayDates(List<PublicHolidayDto> country1Holidays) {
        return country1Holidays.stream()
                .map(PublicHolidayDto::getDate)
                .collect(Collectors.toList());
    }

    private String extractHolidayName(List<PublicHolidayDto> holidays, LocalDate firstCommonDate) {
        return holidays.stream()
                .filter(h -> h.getDate().equals(firstCommonDate))
                .map(PublicHolidayDto::getLocalName)
                .findFirst().orElse(null);
    }

    private List<PublicHolidayDto> getHolidaysForCountry(String countryCode) {

        ResponseEntity<List<PublicHolidayDto>> nextHolidaysResponseV2 = restTemplate.exchange(
                nextHolidaysUrl + "/" + countryCode,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                }
        );
        return nextHolidaysResponseV2.getBody();
    }
}
