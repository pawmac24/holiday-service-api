package com.pm.example.api.controller;

import com.pm.example.api.dto.HolidayResponse;
import com.pm.example.api.service.HolidayService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/holidays")
public class HolidayController {

    private final HolidayService holidayService;

    @GetMapping(value = "/nextCommonHoliday/{startDate}/{countryCode1}/{countryCode2}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HolidayResponse nextCommonHoliday(
            @PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @PathVariable("countryCode1") String countryCode1,
            @PathVariable("countryCode2") String countryCode2
            ) {
        return holidayService.getNextCommonHoliday(startDate, countryCode1, countryCode2);
    }
}
