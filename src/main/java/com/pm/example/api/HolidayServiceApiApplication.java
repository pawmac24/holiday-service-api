package com.pm.example.api;

import net.skobow.auth.apikey.autoconfigure.EnableApiKeyAuthentication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableApiKeyAuthentication
public class HolidayServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HolidayServiceApiApplication.class, args);
	}

}
