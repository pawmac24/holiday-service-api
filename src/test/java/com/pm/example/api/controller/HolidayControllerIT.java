package com.pm.example.api.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.PostConstruct;

import static io.restassured.RestAssured.given;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HolidayControllerIT {

    @LocalServerPort
    private int port;

    private String uri;

    @Value("${web.authentication.apikey}")
    private String apiKey;

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @Test
    public void whenUsedApiKey_and_correct_date_and_countries_thenOK() {

        given()
            .header("X-Api-Key", apiKey)
        .when()
            .get(uri + "/api/holidays/nextCommonHoliday/2021-04-10/PL/DE")
        .then()
            .assertThat()
            .statusCode(HttpStatus.OK.value())
            .body("date", Matchers.equalTo("2021-05-01"))
            .body("name1", Matchers.equalTo("Święto Pracy"))
            .body("name2", Matchers.equalTo("Tag der Arbeit"));
    }

    @Test
    public void whenYouForgetApiKey_thenUnauthorized() {

        given()
        .when()
            .get(uri + "/api/holidays/nextCommonHoliday/2021-04-10/PL/DE")
        .then()
            .assertThat()
            .statusCode(HttpStatus.UNAUTHORIZED.value())
            .body("status", Matchers.equalTo(401))
            .body("error", Matchers.equalTo("Unauthorized"));
    }

    @Test
    public void whenYouGiveBadCountryCode_thenNotFound() {

        given()
            .header("X-Api-Key", apiKey)
        .when()
            .get(uri + "/api/holidays/nextCommonHoliday/2021-04-10/PL/XX")
        .then()
            .assertThat()
            .statusCode(HttpStatus.NOT_FOUND.value())
            .body("message", Matchers.equalTo("Country XX not found"));
    }
}
