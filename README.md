The Holiday information service application user config server
to manage and refresh properties during running holiday-service-api. 
The config server with managed properties are located here: 
- https://gitlab.com/pawmac24/config-server-demo.git
- https://gitlab.com/pawmac24/spring-cloud-config.git

### Running and compiling from scripts or run like below: 
- `./run_local.sh` for local profile
- `./run_dev.sh` for dev profile 
### ... or override web.authentication.apikey
- `mvnw clean package -Pdev && java -jar -Dspring.active.profiles=local -Dweb.authentication.apikey=abc target/holiday-service-api.jar`

###List of given profiles
<pre>
{profile}:
  local (default),
  dev,
  prod,
  test - running test
</pre>

### To compile - choose one option
- `mvnw clean package`
- `mvnw clean package -P{profile}`

You can choose profile according to application-{profile}.properties.
If you do not choose the local profile is default.

### Run config server (optional) before running holiday-service-api
If you do not run server the holiday-service-api will use only local properties
Other profile files are in gitlab https://gitlab.com/pawmac24/spring-cloud-config.git
- `java -jar target/config-server-demo.jar`

### To run choose one option:
- `java -jar target/holiday-service-api.jar`
- `java -jar -Dspring.active.profiles={profile} target/holiday-service-api.jar`
- `java -jar -Dweb.authentication.apikey=abc target/holiday-service-api.jar`
- `java -jar -Dspring.active.profiles={profile} -Dweb.authentication.apikey=abc target/holiday-service-api.jar`
- `mvnw clean spring-boot:run`
- `HolidayServiceApiApplication` in Intellij adding optionally profile in configuration

Choose profile (on the top of the page)

You can also add application.properties in /config folder or main folder of home project folder to override properties
and then run `java -jar target/holiday-service-api.jar` like here: https://www.baeldung.com/spring-properties-file-outside-jar

### Testing the API after run :
In order to test the application use POSTMAN or curl.
You have to define header  
`X-Api-Key: exampleKey` <br />
exampleKey is defined in `web.authentication.apikey` inside application-{profile}.properties or on config server gitlab
holiday-service-api-{profile}.properties. See https://gitlab.com/pawmac24/spring-cloud-config.git

<pre>
curl -X GET "http://localhost:8080/api/holidays/nextCommonHoliday/2021-04-10/PL/DE" -H "X-Api-Key:exampleKey"
</pre>

### Possible response
- Status 200, OK
<pre>
curl -X GET "http://localhost:8080/api/holidays/nextCommonHoliday/2021-04-10/PL/DE" -H "X-Api-Key:exampleKey"

{
  "date": "2021-05-01",
  "name1": "Święto Pracy",
  "name2": "Tag der Arbeit"
}
</pre>

- Status 404, Not found for country XX 
<pre>
curl -X GET "http://localhost:8080/api/holidays/nextCommonHoliday/2021-04-10/PL/XX" -H "X-Api-Key:exampleKey"

{
  "timestamp": "2021-04-11T21:09:30.1815436",
  "message": "Country XX not found"
}
</pre>

- Status 401, Unauthorized when you do not give header X-Api-Key
<pre>
curl -X GET "http://localhost:8080/api/holidays/nextCommonHoliday/2021-04-10/PL/DE"

{
  "timestamp": "2021-04-11T19:14:51.278+00:00",
  "status": 401,
  "error": "Unauthorized",
  "message": "",
  "path": "/api/holidays/nextCommonHoliday/2021-04-10/PL/DE"
}
</pre>


## Build docker image and run it 
1. `docker build .`
2. `docker run -d -p 8080:8080 <id-image>`
3. `docker ps -a`
4. `docker logs <id-container>`
